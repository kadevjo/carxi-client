using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Kadevjo.Dependencies;
using Xamarin.Forms;
using System.Threading.Tasks;
using Xamarin.Geolocation;

[assembly:Dependency(typeof(Kadevjo.iOS.Dependencies.ToolsService))]
namespace Kadevjo.iOS.Dependencies
{
    public class ToolsService : IToolsService
    {
		public ToolsService()
		{
			Console.Write("");
		}

		public void ClearNotifications()
		{
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
			UIApplication.SharedApplication.CancelAllLocalNotifications ();
		}

        public void RefreshActionBar(Page page)
        {
            //This does not apply for iOS
            return;
        }

        public async Task<IDictionary<string, double>> GetUserLocation()
        {
            var locator = new Geolocator() { DesiredAccuracy = 1000 };
            try
            {
                var position = await locator.GetPositionAsync(timeout: 10000);
                var dictionary = new Dictionary<string, double>();
                dictionary.Add("Latitude", position.Latitude);
                dictionary.Add("Longitude", position.Longitude);
                return dictionary;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Dictionary<string, double>();
            }
        }

		public string GetPhoneNumber()
		{
			return string.Empty;
		}
    }
}