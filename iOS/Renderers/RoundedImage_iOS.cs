﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Carxi;

namespace Carxi.iOS
{
	public class RoundedImage_iOS : ImageRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			var image = (RoundedImage)Element;
			Control.Layer.CornerRadius = image.Radius;

			base.OnElementChanged (e);
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if( e.PropertyName == RoundedImage.RadiusProperty.PropertyName ) {
				var image = (RoundedImage)Element;
				Control.Layer.CornerRadius = image.Radius;
			}

			base.OnElementPropertyChanged (sender, e);
		}
	}
}

