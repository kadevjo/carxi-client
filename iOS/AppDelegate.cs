﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

using Parse;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

using Apps = Xamarin.Forms.Application;

namespace Carxi.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();
			global::Xamarin.FormsMaps.Init();

			//
			var dummy = new Kadevjo.iOS.Dependencies.ToolsService();

			ParseClient.Initialize ( Settings.ParseAppId, Settings.ParseAppKey );

			// Code for starting up the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
			#endif

			LoadApplication (new App ());
			RegisterNotifications ();

			// This is for targeting pushes to this specific user
			MessagingCenter.Subscribe<Apps,string> (this, Settings.PhoneAddedKey, (sender, phone) => {
				try {
					ParseInstallation currentInstallation = ParseInstallation.CurrentInstallation;
					currentInstallation.Add("phone", phone);
					currentInstallation.SaveAsync();
				}
				catch (Exception e) {
					Console.WriteLine(e.Message);
				}
			});

			return base.FinishedLaunching (app, options);
		}

		public void RegisterNotifications()
		{
			UIApplication app = UIApplication.SharedApplication;

			var version = new Version (UIDevice.CurrentDevice.SystemVersion);
			if (version >= new Version ("8.0")) {
				UIUserNotificationType notificationTypes = 
					UIUserNotificationType.Alert | 
					UIUserNotificationType.Badge | 
					UIUserNotificationType.Sound;
				UIUserNotificationSettings settings = UIUserNotificationSettings.GetSettingsForTypes (
					notificationTypes, 
					null
				);
				app.RegisterUserNotificationSettings (settings);
				app.RegisterForRemoteNotifications ();
			} 
			else {
				UIRemoteNotificationType notificationTypes = 
					UIRemoteNotificationType.Alert |
					UIRemoteNotificationType.Badge | 
					UIRemoteNotificationType.Sound;
				app.RegisterForRemoteNotificationTypes(notificationTypes);
			}
		}

		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
			try
			{
				ParseInstallation currentInstallation = ParseInstallation.CurrentInstallation;
				currentInstallation.SetDeviceTokenFromData(deviceToken);
				currentInstallation.SaveAsync();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		public override void DidReceiveRemoteNotification (UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			application.ApplicationIconBadgeNumber = 0;

			Dictionary<string, object> payload = new Dictionary<string, object>();
			foreach ( var key in userInfo.Keys ) {
				payload.Add(
					key.ToString(),
					userInfo.ObjectForKey( key )
				);
			}

			if (application.ApplicationState == UIApplicationState.Active) {                
				MessagingCenter.Send<Apps,Dictionary<string,object>> ( 
					App.Current, Settings.TripEndKey, payload 
				);
			}  
			else {
				CreateNotification ( "Carxi", "Tu trip ha terminado", payload);
			}
		}

		private void CreateNotification(string title, string description, Dictionary<string,object> payload)
		{
			App.Args = payload;
		}
	}
}

