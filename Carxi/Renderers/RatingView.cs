﻿using System;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace Carxi
{
	public class RatingView : ContentView
	{
		//Maybe for a later custom implementation using reflection 
		//private const int RATING_METTER = 5; //Should be a property

		#region Private members
		private Image star1;
		private Image star2;
		private Image star3;
		private Image star4;
		private Image star5;
		private TapGestureRecognizer tapStar1;
		private TapGestureRecognizer tapStar2;
		private TapGestureRecognizer tapStar3;
		private TapGestureRecognizer tapStar4;
		private TapGestureRecognizer tapStar5;
		#endregion

		#region Bindable Properties
		public static BindableProperty RateValueProperty = 
			BindableProperty.Create<RatingView, int>(r => r.RateValue, 0);
		public static BindableProperty IconActiveResourceProperty =
			BindableProperty.Create<RatingView, string>(r => r.IconActiveResource, string.Empty);
		public static BindableProperty IconDeactiveResourceProperty =
			BindableProperty.Create<RatingView, string>(r => r.IconDeactiveResource, string.Empty);
		public static BindableProperty IsEditableProperty =
			BindableProperty.Create<RatingView, bool>(r => r.IsEditable, true);
		#endregion

		#region Properties
		public int RateValue
		{
			get { return (int)GetValue(RateValueProperty); }
			set { SetValue(RateValueProperty, value); }
		}
		public string IconActiveResource
		{
			get { return (string)GetValue(IconActiveResourceProperty); }
			set { SetValue(IconActiveResourceProperty, value); }
		}
		public string IconDeactiveResource
		{
			get { return (string)GetValue(IconDeactiveResourceProperty); }
			set { SetValue(IconDeactiveResourceProperty, value); }
		}
		public bool IsEditable
		{
			get { return (bool)GetValue(IsEditableProperty); }
			set { SetValue(IsEditableProperty, value); }
		}
		#endregion

		public event EventHandler<RateValueEventArgs> RateValueChanged;


		public RatingView()
		{
			InitializeComponents();
			RelativeLayout layout = new RelativeLayout ();
			layout.Children.Add(star1,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((p) => p.Width / 5),
				Constraint.RelativeToParent((p) => p.Height));
			layout.Children.Add(star2,
				Constraint.RelativeToView(star1, (p, v) => v.X + v.Width),
				Constraint.RelativeToView(star1, (p, v) => v.Y),
				Constraint.RelativeToParent((p) => p.Width / 5),
				Constraint.RelativeToParent((p) => p.Height));
			layout.Children.Add(star3,
				Constraint.RelativeToView(star2, (p, v) => v.X + v.Width),
				Constraint.RelativeToView(star1, (p, v) => v.Y),
				Constraint.RelativeToParent((p) => p.Width / 5),
				Constraint.RelativeToParent((p) => p.Height));
			layout.Children.Add(star4,
				Constraint.RelativeToView(star3, (p, v) => v.X + v.Width),
				Constraint.RelativeToView(star1, (p, v) => v.Y),
				Constraint.RelativeToParent((p) => p.Width / 5),
				Constraint.RelativeToParent((p) => p.Height));
			layout.Children.Add(star5,
				Constraint.RelativeToView(star4, (p, v) => v.X + v.Width),
				Constraint.RelativeToView(star1, (p, v) => v.Y),
				Constraint.RelativeToParent((p) => p.Width / 5),
				Constraint.RelativeToParent((p) => p.Height));
			Content = layout;
		}

		private void InitializeComponents()
		{
			#region Initialize controls
			star1 = new Image
			{
				Aspect = Aspect.AspectFit,
				Source = IconDeactiveResource
			};
			star2 = new Image
			{
				Aspect = Aspect.AspectFit,
				Source = IconDeactiveResource,
			};
			star3 = new Image
			{
				Aspect = Aspect.AspectFit,
				Source = IconDeactiveResource
			};
			star4 = new Image
			{
				Aspect = Aspect.AspectFit,
				Source = IconDeactiveResource
			};
			star5 = new Image
			{
				Aspect = Aspect.AspectFit,
				Source = IconDeactiveResource
			};
			#endregion

			#region Set active stars
			SetActiveStars();
			#endregion

			#region Set Gesture recognizers
			SetGestureRecognizers();
			#endregion
		}

		private void SetActiveStars()
		{
			if (RateValue > 0)
			{
				for (int i = 1; i <= 5; i++)
				{
					switch (i)
					{
					case 1:
						if (RateValue >= i)
							star1.Source = IconActiveResource;
						else
							star1.Source = IconDeactiveResource;
						break;
					case 2:
						if (RateValue >= i)
							star2.Source = IconActiveResource;
						else
							star2.Source = IconDeactiveResource;
						break;
					case 3:
						if (RateValue >= i)
							star3.Source = IconActiveResource;
						else
							star3.Source = IconDeactiveResource;
						break;
					case 4:
						if (RateValue >= i)
							star4.Source = IconActiveResource;
						else
							star4.Source = IconDeactiveResource;
						break;
					case 5:
						if (RateValue >= i)
							star5.Source = IconActiveResource;
						else
							star5.Source = IconDeactiveResource;
						break;
					}
				}
			}
			else
			{
				star1.Source = IconDeactiveResource;
				star2.Source = IconDeactiveResource;
				star3.Source = IconDeactiveResource;
				star4.Source = IconDeactiveResource;
				star5.Source = IconDeactiveResource;
			}
		}

		private void SetGestureRecognizers()
		{
			star1.GestureRecognizers.Clear();
			star2.GestureRecognizers.Clear();
			star3.GestureRecognizers.Clear();
			star4.GestureRecognizers.Clear();
			star5.GestureRecognizers.Clear();
			if (IsEditable)
			{
				tapStar1 = new TapGestureRecognizer();
				tapStar2 = new TapGestureRecognizer();
				tapStar3 = new TapGestureRecognizer();
				tapStar4 = new TapGestureRecognizer();
				tapStar5 = new TapGestureRecognizer();
				tapStar1.Tapped += (sender, e) => RateValue = 1;
				tapStar2.Tapped += (sender, e) => RateValue = 2;
				tapStar3.Tapped += (sender, e) => RateValue = 3;
				tapStar4.Tapped += (sender, e) => RateValue = 4;
				tapStar5.Tapped += (sender, e) => RateValue = 5;
				star1.GestureRecognizers.Add(tapStar1);
				star2.GestureRecognizers.Add(tapStar2);
				star3.GestureRecognizers.Add(tapStar3);
				star4.GestureRecognizers.Add(tapStar4);
				star5.GestureRecognizers.Add(tapStar5);
			}
		}

		protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if(propertyName == RateValueProperty.PropertyName)
			{
				if(RateValueChanged != null)
					RateValueChanged(this, new RateValueEventArgs(RateValue));
				SetActiveStars();
			}
			else if (propertyName == IconActiveResourceProperty.PropertyName)
			{
				SetActiveStars();
			}
			else if (propertyName == IconDeactiveResourceProperty.PropertyName)
			{
				SetActiveStars();
			}
			else if(propertyName == IsEditableProperty.PropertyName)
			{
				SetGestureRecognizers();
			}
		}
	}

	public class RateValueEventArgs : EventArgs
	{
		public int RateValue;

		public RateValueEventArgs(int index)
		{
			RateValue = index;
		}
	}
}