﻿using System;
using Xamarin.Forms;

namespace Carxi
{
	public class RoundedImage : Image
	{
		public static readonly BindableProperty RadiusProperty = 
			BindableProperty.Create<RoundedImage, int> (
				x => x.Radius, 0
			);

		public int Radius {
			get { 
				return (int)GetValue ( RadiusProperty );
			}
			set { 
				SetValue ( RadiusProperty, value );
			}
		}
	}
}

