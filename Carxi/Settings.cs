﻿using System;
using Xamarin.Forms;

namespace Carxi
{
	public static class Settings 
	{
		public static readonly string AppName = "Carxi";
		public static readonly string BaseUrl = "https://taxioapp.com/api/v1/";
		public static readonly Color Color1 = Color.FromHex("73C8A9");
		public static readonly Color Color2 = Color.FromHex("DEE1B6");
		public static readonly Color Color3 = Color.FromHex("E1B866");
		public static readonly Color Color4 = Color.FromHex("BD5532");
		public static readonly Color Color5 = Color.FromHex("373B44");
		public static readonly Location DefaultLocation = new Location( 38.89511, -77.03637 );

		#region Setting Keys

		public static readonly string PhoneKey = "Phone";
		public static readonly string PushKey = "Push";

		#endregion

		#region Message Keys

		public static readonly string PhoneAddedKey = "PhoneAdded";
		public static readonly string TripEndKey = "TripEnd";

		#endregion

		#region Service Keys

		public static readonly string ParseAppId = "CDOXE5kp4wznXJl9XgcN3zcRT475hwM5eZXrtn1T";
		public static readonly string ParseAppKey = "5sGoZILg68HgFB1Tx7DSuJK2ldF902agzZIpAWAG";

		#endregion
	}
}

