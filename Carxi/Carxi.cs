﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Kadevjo.Dependencies;

namespace Carxi
{
	public class App : Application
	{
		public static Dictionary<string,object> Args;

		public App ()
		{
			// The root page of your application
			MainPage = new NavigationPage( new MapPage() ) {
				BarBackgroundColor = Settings.Color1,
				BarTextColor = Color.White
			};
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
			DependencyService.Get<IToolsService> ().ClearNotifications();
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// If app was closed and a "trip ends" push arrives
			if (App.Args != null) {
				MainPage.Navigation.PushModalAsync(new RatingPage(App.Args));
				App.Args = null;
			}
		}
	}
}

