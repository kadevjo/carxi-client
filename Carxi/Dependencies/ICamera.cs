﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Carxi
{
	public interface ICamera
	{
		Task<Stream> TakePhoto();
		Task<Stream> PickPhoto();

	}
}

