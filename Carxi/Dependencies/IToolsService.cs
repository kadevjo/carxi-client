﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Kadevjo.Dependencies
{
    public interface IToolsService
    {
		void ClearNotifications ();
        void RefreshActionBar(Page page);
        Task<IDictionary<string, double>> GetUserLocation();
		string GetPhoneNumber();
    }
}
