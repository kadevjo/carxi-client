﻿using System;
using BindableMapTest.Interfaces;

namespace Carxi
{
	public class Location : ILocation
	{
		public Location()
		{
		}

		public Location( double latitude, double longitude )
		{
			Latitude = latitude;
			Longitude = longitude;
		}

		public double Latitude { get; set; }
		public double Longitude { get; set; }
	}
}

