﻿using System;
using BindableMapTest.Interfaces;

using Newtonsoft.Json;

namespace Carxi
{
	public class Cab : IMapModel
	{
		public string Id { get; set; }
		public string Description { get; set; }
		public string FullName { get; set; }
		public Location Coordinates { get; set; }
		public string Color { get; set; }
		[JsonProperty("tag_number")]
		public string Tag { get; set; }
		[JsonProperty("driver_picture")]
		public string DriverPicture { get; set; }

		#region IMapModel implementation

		public string Name {
			get {
				return FullName ?? "";
			}
			set {
				FullName = value;
			}
		}

		public string Details {
			get {
				return Description;
			}
			set {
				Description = value;
			}
		}

		public ILocation Location {
			get {
				return Coordinates;
			}
			set {
				Coordinates = (Location)value;
			}
		}

		public string PinIcon {
			get {
				return "ic_taxi_location";
			}
			set {
				//
			}
		}

		#endregion
	}
}

