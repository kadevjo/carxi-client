﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Carxi
{
	public class Result
	{
		public string PlaceId { get; set; }
		[JsonProperty("formatted_address")]
		public string FormattedAddress { get; set; }
	}

	public class GoogleMapsResponse
	{
		public List<Result> Results { get; set; }
		public string Status { get; set; }
	}
}
