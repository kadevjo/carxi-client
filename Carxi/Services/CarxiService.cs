﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.Diagnostics;

using RestSharp.Portable;
using Newtonsoft.Json;

using Kadevjo;

namespace Carxi
{
	public class CabsResponse 
	{
		public List<Cab> Cabs { get; set; }
	}

	public class RequestResponse 
	{
		public bool Success { get; set; }
		[JsonProperty("trip_id")]
		public string Trip { get; set; }
		public string Message { get; set; }
		public string Error { get; set; }
	}

	public class TripResponse
	{
		public string Id { get; set; }
		public bool Assigned { get; set; }
		public string Pickup { get; set; }
		[JsonProperty("pickup_at")]
		public string PickupAt { get; set; }
		public Cab Cab { get; set; }
	}

	public class CarxiService: RestService<CarxiService,Cab>
	{
		protected override string BaseUrl {
			get {
				return Settings.BaseUrl;
			}
		}

		protected override string Resource {
			get {
				return "companies/13abc6ca-0b36-4567-a3d5-08f4c22e6e83/cabs_elegible_for_autodispatch.json";
			}
		}

		protected override Dictionary<string, string> Headers {
			get {
				return new Dictionary<string,string>() {
					{"Authorization","Token token=b7da8657f01946aa9ab0fbf23a881114"}
				};
			}
		}

		public override async Task<List<Cab>> ReadAll ()
		{
			var response = await Execute<CabsResponse> ( Resource );
			return response.Cabs;
		}

		public async Task<RequestResponse> CreateTrip( string phone, string address )
		{
			RestRequest request = new RestRequest ( "carxi/trips", HttpMethod.Post );
			request.AddParameter ( "phone", phone );
			request.AddParameter ( "pickup", address );
			request.AddParameter ( "pickup_at", DateTime.Now );

			try {
				var response = await Client.Execute<RequestResponse>( request );

				if( response.StatusCode == HttpStatusCode.OK ) {
					return response.Data;
				}
				else {
					return default( RequestResponse );
				}
			}
			catch ( Exception e ) {
				Debug.WriteLine ( e.Message );
				return default( RequestResponse );
			}
		}

		public async Task<TripResponse> GetTrip( string id )
		{
			RestRequest request = new RestRequest ( string.Format("carxi/trips/{0}.json",id), HttpMethod.Get );

			try {
				var response = await Client.Execute<TripResponse>( request );

				if( response.StatusCode == HttpStatusCode.OK ) {
					return response.Data;
				}
				else {
					return default( TripResponse );
				}
			}
			catch ( Exception e ) {
				Debug.WriteLine ( e.Message );
				return default( TripResponse );
			}
		}

		public async Task<bool> CallDriver(string id)
		{
			RestRequest request = new RestRequest(string.Format("carxi/trips/{0}/call_bridge_client_to_driver", id), HttpMethod.Post);
		
			try
			{
				var response = await Client.Execute<RequestResponse>(request);

				if (response.StatusCode == HttpStatusCode.OK) {
					return response.Data.Success;
				}
				else {
					return false;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
				return false;
			}
		}

		public async Task<RequestResponse> CancelTrip( string id )
		{
			RestRequest request = new RestRequest ( string.Format("carxi/trips/{0}/cancel",id), HttpMethod.Put );

			try {
				var response = await Client.Execute<RequestResponse>( request );

				if( response.StatusCode == HttpStatusCode.OK ) {
					return response.Data;
				}
				else {
					return default( RequestResponse );
				}
			}
			catch ( Exception e ) {
				Debug.WriteLine ( e.Message );
				return default( RequestResponse );
			}
		}

		public async Task<bool> RateTrip( string id, int rating )
		{
			RestRequest request = new RestRequest ( string.Format("carxi/trips/{0}/client_rating",id), HttpMethod.Put );

			try {
				var response = await Client.Execute( request );

				if( response.StatusCode == HttpStatusCode.OK ) {
					return true;
				}
				else {
					return false;
				}
			}
			catch ( Exception e ) {
				return false;
			}
		}
	}
}

