﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Kadevjo;

namespace Carxi
{
	public class GoogleMapsService : RestService<GoogleMapsService,object>
	{
		protected override string BaseUrl {
			get {
				return "https://maps.googleapis.com/maps/api/";
			}
		}

		protected override string Resource {
			get {
				return "geocode/json";
			}
		}

		protected override Dictionary<string, string> Headers {
			get {
				return null;
			}
		}

		public async Task<List<Result>> GetNearbyPlaces (double latitude, double longitude)
		{
			IQuery query = new RestQuery ();
			query.Add ( "latlng", string.Format("{0},{1}",latitude,longitude) );
			query.Add ( "sensor", true );

			var response = await Execute<GoogleMapsResponse> ( Resource, query );
			return response.Results;
		}
	}
}

