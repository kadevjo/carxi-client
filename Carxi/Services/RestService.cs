﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using RestSharp.Portable;

namespace Kadevjo
{
	public class RestQuery : IQuery
	{
		public RestQuery()
		{
			parameters = new Dictionary<string, object> ();
		}

		private Dictionary<string, object> parameters;

		public Dictionary<string, object> Parameters {
			get {
				return parameters;
			}
		}

		public IQuery Add (string property, object value)
		{
			parameters.Add ( property, value );
			return this;
		}

		public IQuery Equal (string property, object value)
		{
			throw new NotImplementedException ();
		}

		public IQuery NotEqual (string property, object value)
		{
			throw new NotImplementedException ();
		}

		public IQuery GreaterThan (string property, object value)
		{
			throw new NotImplementedException ();
		}

		public IQuery GreaterThanOrEqual (string property, object value)
		{
			throw new NotImplementedException ();
		}

		public IQuery LowerThan (string property, object value)
		{
			throw new NotImplementedException ();
		}

		public IQuery LowerThanOrEqual (string property, object value)
		{
			throw new NotImplementedException ();
		}

		public IQuery ContainedIn (string property, object value)
		{
			throw new NotImplementedException ();
		}

		public IQuery NotContainedIn (string property, object value)
		{
			throw new NotImplementedException ();
		}

		public IQuery Skip (int value)
		{
			throw new NotImplementedException ();
		}

		public IQuery Limit (int value)
		{
			throw new NotImplementedException ();
		}

		public IQuery OrderBy (string property)
		{
			throw new NotImplementedException ();
		}

		public IQuery OrderByDescending (string property)
		{
			throw new NotImplementedException ();
		}
	}

	public abstract class RestService<I,T> : IService<T> where I : new()
	{
		private static I instance;
		public static I Instance {
			get { 
				if( instance == null ) {
					instance = new I ();
				}

				return instance;
			}
		}

		protected RestClient client;
		protected RestClient Client {
			get { 
				if( client == null ) {
					client = new RestClient ( BaseUrl );

					if( Headers != null && Headers.Count > 0 ) {
						foreach( var header in Headers ) {
							client.AddDefaultParameter ( 
								header.Key, 
								header.Value, 
								ParameterType.HttpHeader 
							);
						}
					}
				}

				return client;
			}
		}

		protected abstract string BaseUrl { get; }
		protected abstract Dictionary<string,string> Headers { get; }
		protected abstract string Resource { get; }

		#region IService implementation

		public virtual async Task<bool> Create (T entity)
		{
			return await Execute<T> ( Resource, HttpMethod.Post, entity );
		}

		public virtual async Task<T> Read (string id)
		{
			string resource = Resource + "/" + id;
			return await Execute<T> (resource);
		}

		public virtual async Task<List<T>> ReadAll ()
		{
			return await Execute<List<T>> (Resource);
		}

		public virtual async Task<List<T>> ReadAll (IQuery query)
		{
			return await Execute<List<T>> ( Resource, query );
		}

		public virtual async Task<bool> Update (T entity)
		{
			return await Execute<T> ( Resource, HttpMethod.Put, entity );
		}

		public virtual async Task<bool> Delete (string id)
		{
			string resource = Resource + "/" + id;
			return await Execute<T> ( resource, HttpMethod.Delete );
		}

		public virtual async Task<R> Execute<R> (string resource, IQuery query = null)
		{
			RestRequest request = new RestRequest ( resource, HttpMethod.Get );

			if ( query != null && query.Parameters != null && query.Parameters.Count > 0 ) {
				foreach (var param in query.Parameters) {
					request.AddParameter (
						param.Key, 
						param.Value, 
						ParameterType.GetOrPost
					);
				}
			}

			try {
				var response = await Client.Execute<R>( request );

				if( response.StatusCode == HttpStatusCode.OK ) {
					return response.Data;
				}
				else {
					return default( R );
				}
			}
			catch ( Exception e ) {
				Debug.WriteLine ( e.Message );
				return default( R );
			}
		}

		public virtual async Task<bool> Execute<R>( string resource, HttpMethod method, R body = default(R) ) 
		{
			RestRequest request = new RestRequest ( resource, method );

			if (body != null) {
				request.AddBody (body);
			}

			try {
				var response = await Client.Execute( request );

				if( response.StatusCode == HttpStatusCode.OK ) {
					return true;
				}
				else {
					return false;
				}
			}
			catch ( Exception e ) {
				Debug.WriteLine ( e.Message );
				return false;
			}
		}

		#endregion
	}
}

