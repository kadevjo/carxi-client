﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

using Kadevjo.Dependencies;

namespace Carxi
{
	public partial class MapPage : ContentPage
	{
		private Label addressLabel;
		private Entry addressEntry;
		private Button continueButton;

		private void InitializeRequestMode ()
		{
			// Updating mode layout
			if(modeLayout != null && layout.Children.Contains( modeLayout ) ) {
				layout.Children.Remove ( modeLayout );
			}

			modeLayout = new RelativeLayout () { 
				BackgroundColor = Color.White.MultiplyAlpha( 0.6 ),
				AnchorX = 0.5,
				AnchorY = 0.5
			};

			addressLabel = new Label () {
				Text = "¿Dónde lo recogemos?",
				TextColor = Settings.Color5,
				FontSize = 15,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center
			};

			modeLayout.Children.Add (
				addressLabel,
				Constraint.Constant( 0 ),
				Constraint.Constant( 10 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 25 )
			);

			addressEntry = new Entry() {
				Placeholder = "Cargando su dirección...",
				BackgroundColor = Device.OnPlatform<Color>( Color.White, Color.Silver, Color.White ),
				TextColor = Settings.Color5
			};

			modeLayout.Children.Add (
				addressEntry,
				Constraint.RelativeToParent( (p) => p.Width * 0.05 ),
				Constraint.RelativeToView( addressLabel, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.9 ),
				Constraint.Constant( 50 )
			);

			continueButton = new Button () { 
				Text = "Continuar",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Settings.Color5,
				BackgroundColor = Settings.Color3,
				BorderRadius = 0
			};

			modeLayout.Children.Add (
				continueButton,
				Constraint.RelativeToParent( (p) => p.Width * 0.05 ),
				Constraint.RelativeToView( addressEntry, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.9 ),
				Constraint.Constant( 44 )
			);

			int height = Device.OnPlatform<int> ( 160, 160, 160 );

			layout.Children.Add (
				modeLayout,
				Constraint.Constant( 0 ),
				Constraint.RelativeToParent( (p) => p.Height - height ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( height )
			);

			continueButton.Clicked += SelectAddress;
			addressEntry.Focused += EntryFocused;
			LoadAddres ().ConfigureAwait ( false );
		}

		private async Task LoadAddres()
		{
			var location = await DependencyService.Get<IToolsService>().GetUserLocation();
			double latitude = 0;
			double longitude = 0;
			location.TryGetValue( "Latitude", out latitude );
			location.TryGetValue( "Longitude", out longitude );

			var places = await GoogleMapsService.Instance.GetNearbyPlaces ( latitude, longitude );

			Device.BeginInvokeOnMainThread ( () => {
				if( places != null && places.Count > 0 ) {
					addressEntry.Text = places[0].FormattedAddress;
				}
			});
		}

		private async Task RequestModeAppear()
		{
			modeLayout.TranslationY = modeLayout.Height;
			addressLabel.Opacity = 0;
			addressEntry.Opacity = 0;
			continueButton.Opacity = 0;

			await modeLayout.TranslateTo ( 0, 0, 438 );
			await addressLabel.FadeTo ( 1 );
			await addressEntry.FadeTo ( 1 );
			await continueButton.FadeTo ( 1 );
		}

		private async Task RequestModeDissapear()
		{
			await addressLabel.FadeTo ( 0 );
			await addressEntry.FadeTo ( 0 );
			await continueButton.FadeTo ( 0 );
			await modeLayout.TranslateTo ( 0, modeLayout.Height, 438 );
		}
	}
}


