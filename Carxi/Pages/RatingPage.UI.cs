﻿using System;

using Xamarin.Forms;

namespace Carxi
{
	public partial class RatingPage : ContentPage
	{
		private Image topImage;
		private Label priceLabel;
		private Label totalLabel;
		private Label ratingLabel;
		private RatingView tripRating;
		private Button sendButton;
		private Button cancelButton;

		private void InitializeComponents()
		{
			RelativeLayout layout = new RelativeLayout ();

			topImage = new Image () { 
				Source = "ic_order_taxi.png",
				Aspect = Aspect.AspectFit
			};

			layout.Children.Add (
				topImage,
				Constraint.RelativeToParent( (p) => p.Width / 2 - 75 ),
				Constraint.RelativeToParent( (p) => p.Height / 2 - 75 - 120 ),
				Constraint.Constant( 150 ),
				Constraint.Constant( 150 )
			);

			priceLabel = new Label () { 
				Text = "Su total a pagar es:",
				TextColor = Settings.Color5,
				FontSize = 14,
				XAlign = TextAlignment.Center
			};

			layout.Children.Add (
				priceLabel,
				Constraint.Constant( 0 ),
				Constraint.RelativeToView( topImage, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 18 )
			);

			totalLabel = new Label () { 
				Text = fare,
				TextColor = Settings.Color5,
				FontSize = 20,
				XAlign = TextAlignment.Center
			};

			layout.Children.Add (
				totalLabel,
				Constraint.Constant( 0 ),
				Constraint.RelativeToView( priceLabel, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 22 )
			);

			ratingLabel = new Label () { 
				Text = "Califique su experiencia en Carxi",
				TextColor = Settings.Color5,
				FontSize = 14,
				XAlign = TextAlignment.Center
			};

			layout.Children.Add (
				ratingLabel,
				Constraint.Constant( 0 ),
				Constraint.RelativeToView( totalLabel, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 18 )
			);

			tripRating = new RatingView() {
				IsEditable = true,
				IconActiveResource = "ic_star_favon.png",
				IconDeactiveResource = "ic_star_favoff.png"
			};

			layout.Children.Add (tripRating,
				Constraint.RelativeToParent( (p) => p.Width * 0.25),
				Constraint.RelativeToView (ratingLabel, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent( (p) => p.Width * 0.5),
				Constraint.Constant( 60 )
			);

			sendButton = new Button () { 
				Text = "Enviar",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Settings.Color5,
				BackgroundColor = Settings.Color3,
				BorderRadius = 0
			};

			layout.Children.Add (
				sendButton,
				Constraint.RelativeToParent( (p) => p.Width * 0.05 ),
				Constraint.RelativeToParent( (p) => p.Height - (44 + 10 + 44 +10) ),
				Constraint.RelativeToParent( (p) => p.Width * 0.9 ),
				Constraint.Constant( 44 )
			);

			cancelButton = new Button () { 
				Text = "Cancelar",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Settings.Color5,
				BackgroundColor = Settings.Color3,
				BorderRadius = 0
			};

			layout.Children.Add (
				cancelButton,
				Constraint.RelativeToParent( (p) => p.Width * 0.05 ),
				Constraint.RelativeToView( sendButton, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.9 ),
				Constraint.Constant( 44 )
			);

			sendButton.Clicked += SendRate;
			cancelButton.Clicked += Cancel;

			Content = layout;
		}
	}
}

