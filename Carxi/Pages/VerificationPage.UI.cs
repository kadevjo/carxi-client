﻿using System;

using Xamarin.Forms;

namespace Carxi
{
	public partial class VerificationPage : ContentPage
	{
		private Image topImage;
		private Label titleLabel;
		private Label subtitleLabel;
		private Entry codeEntry;
		private Button verifyButton;
		private Button cancelButton;
		private ScrollView scrollView;

		private void InitializeComponents()
		{
			RelativeLayout layout = new RelativeLayout ();

			topImage = new Image () { 
				Source = "ic_order_taxi.png",
				Aspect = Aspect.AspectFit
			};

			layout.Children.Add (
				topImage,
				Constraint.RelativeToParent( (p) => p.Width / 2 - 75 ),
				Constraint.RelativeToParent( (p) => p.Height / 2 - 75 - 120 ),
				Constraint.Constant( 150 ),
				Constraint.Constant( 150 )
			);

			titleLabel = new Label () { 
				Text = "Ordenando",
				TextColor = Settings.Color5,
				FontSize = 20,
				XAlign = TextAlignment.Center
			};

			layout.Children.Add (
				titleLabel,
				Constraint.Constant( 0 ),
				Constraint.RelativeToView( topImage, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 25 )
			);

			subtitleLabel = new Label () { 
				Text = "Sólo unos detallitos más",
				TextColor = Settings.Color5,
				FontSize = 14,
				XAlign = TextAlignment.Center
			};

			layout.Children.Add (
				subtitleLabel,
				Constraint.Constant( 0 ),
				Constraint.RelativeToView( titleLabel, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 18 )
			);

			codeEntry = new Entry () { 
				Placeholder = "Ingrese su número de celular",
				BackgroundColor = Device.OnPlatform<Color>( Color.White, Color.Silver, Color.White ),
				TextColor = Settings.Color5,
				Keyboard = Keyboard.Telephone
			};

			layout.Children.Add (
				codeEntry,
				Constraint.RelativeToParent( (p) => p.Width * 0.05 ),
				Constraint.RelativeToView( subtitleLabel, (p,v) => v.Y + v.Height + 50 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.9 ),
				Constraint.Constant( 40 )
			);

			verifyButton = new Button () { 
				Text = "Continuar",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Settings.Color5,
				BackgroundColor = Settings.Color3,
				BorderRadius = 0
			};

			layout.Children.Add (
				verifyButton,
				Constraint.RelativeToParent( (p) => p.Width * 0.05 ),
				Constraint.RelativeToView( codeEntry, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.9 ),
				Constraint.Constant( 44 )
			);

			cancelButton = new Button () { 
				Text = "Cancelar",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Settings.Color5,
				BackgroundColor = Settings.Color3,
				BorderRadius = 0
			};

			layout.Children.Add (
				cancelButton,
				Constraint.RelativeToParent( (p) => p.Width * 0.05 ),
				Constraint.RelativeToView( verifyButton, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.9 ),
				Constraint.Constant( 44 )
			);

			verifyButton.Clicked += VerifyPhone;
			cancelButton.Clicked += CancelVerification;
			codeEntry.Focused += EntryFocused;
			codeEntry.TextChanged += EntryChanged;

			scrollView = new ScrollView () { 
				Content = layout
			};

			Content = scrollView;
		}
	}
}


