﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Carxi
{
	public partial class MapPage : ContentPage
	{
		private Button requestButton;
		private Label etaLabel;

		private void InitializeViewMode()
		{
			// Updating mode layout
			if(modeLayout != null && layout.Children.Contains( modeLayout ) ) {
				layout.Children.Remove ( modeLayout );
			}

			modeLayout = new RelativeLayout () { 
				BackgroundColor = Color.White.MultiplyAlpha( 0.8 ),
				AnchorX = 0.5,
				AnchorY = 0.5
			};

			requestButton = new Button () { 
				Text = "Pedir Carxi",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Settings.Color5,
				BackgroundColor = Settings.Color3,
				BorderRadius = 0
			};

			modeLayout.Children.Add (
				requestButton,
				Constraint.RelativeToParent( (p) => p.Width * 0.05 ),
				Constraint.Constant( 10 + 8 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.9 ),
				Constraint.Constant( 44 )
			);

			etaLabel = new Label () { 
				Text = "",
				TextColor = Settings.Color5,
				FontSize = 12,
				XAlign = TextAlignment.Center
			};

			modeLayout.Children.Add (
				etaLabel,
				Constraint.Constant (0),
				Constraint.RelativeToView (requestButton, (p, v) => v.Y + v.Height + 5),
				Constraint.RelativeToParent ((p) => p.Width),
				Constraint.Constant (16)
			);

			layout.Children.Add (
				modeLayout,
				Constraint.Constant( 0 ),
				Constraint.RelativeToParent( (p) => p.Height - 80 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 80 )
			);

			requestButton.Clicked += RequestCab;
			LoadCabs ().ConfigureAwait ( false );
		}

		private async Task LoadCabs()
		{
			var cabs = await CarxiService.Instance.ReadAll ();

			Device.BeginInvokeOnMainThread (() => {
				cabsMap.UpdatePins( cabs );
				SetUserLocation();
			});
		}

		private async Task ViewModeAppear()
		{
			modeLayout.TranslationY = modeLayout.Height;
			requestButton.Opacity = 0;
			etaLabel.Opacity = 0;

			await modeLayout.TranslateTo ( 0, 0 );
			await requestButton.FadeTo ( 1 );
			await etaLabel.FadeTo ( 1 );
		}

		private async Task ViewModeDisappear() 
		{
			await requestButton.FadeTo ( 0 );
			await etaLabel.FadeTo ( 0 );
			await modeLayout.TranslateTo ( 0, modeLayout.Height );
		}
	}
}


