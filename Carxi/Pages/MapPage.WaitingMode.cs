﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Carxi
{
	public partial class MapPage : ContentPage
	{
		private ActivityIndicator loadingIndicator;
		private Label loadingLabel;
		private Image driverImage;
		private Label driverLavel;
		private Label tagLabel;
		private Label timeLabel;
		private Label arrivalLabel;
		private Button callButton;
		private Button cancelButton;

		private void InitializeWaitingMode()
		{
			// Updating mode layout
			if(modeLayout != null && layout.Children.Contains( modeLayout ) ) {
				layout.Children.Remove ( modeLayout );
			}

			modeLayout = new RelativeLayout () { 
				BackgroundColor = Color.White.MultiplyAlpha( 0.6 ),
				AnchorX = 0.5,
				AnchorY = 0.5
			};

			loadingIndicator = new ActivityIndicator () {
				IsRunning = true,
				Color = Settings.Color5
			};

			modeLayout.Children.Add (
				loadingIndicator,
				Constraint.RelativeToParent( (p) => p.Width / 2 - 40 ),
				Constraint.RelativeToParent( (p) => p.Height / 2 - 80 ),
				Constraint.Constant( 60 ),
				Constraint.Constant( 60 )
			);

			loadingLabel = new Label () { 
				Text = "Espere mientras buscamos el Carxi más cercano",
				TextColor = Settings.Color5,
				FontSize = 14,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center
			};

			modeLayout.Children.Add (
				loadingLabel,
				Constraint.RelativeToParent( (p) => p.Width * 0.2 ),
				Constraint.RelativeToView( loadingIndicator, (p,v) => v.Y + v.Height + 5 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.6 ),
				Constraint.Constant( 40 )
			);

			driverImage = new Image () { 
				Source = "user.png",
				Aspect = Aspect.AspectFill
			};

			modeLayout.Children.Add (
				driverImage,
				Constraint.RelativeToParent( (p) => p.Width / 2 - 60 ),
				Constraint.Constant( 10 ),
				Constraint.Constant( 50 ),
				Constraint.Constant( 50 )
			);

			driverLavel = new Label () { 
				Text = "---",
				TextColor = Settings.Color5,
				FontSize = 14,
				FontAttributes = FontAttributes.Bold,
				YAlign = TextAlignment.Center
			};

			modeLayout.Children.Add (
				driverLavel,
				Constraint.RelativeToView( driverImage, (p,v) => v.X + v.Width + 10 ),
				Constraint.RelativeToView( driverImage, (p,v) => v.Y ),
				Constraint.RelativeToView( driverImage, (p,v) => p.Width - ( v.X + v.Width + 10 + 10 ) ),
				Constraint.RelativeToView( driverImage, (p,v) => v.Height / 2 )
			);

			tagLabel = new Label () { 
				Text = "TAG: ---",
				TextColor = Settings.Color5,
				FontSize = 14,
				FontAttributes = FontAttributes.Bold,
				YAlign = TextAlignment.Center
			};

			modeLayout.Children.Add (
				tagLabel,
				Constraint.RelativeToView( driverImage, (p,v) => v.X + v.Width + 10 ),
				Constraint.RelativeToView( driverImage, (p,v) => v.Y + v.Height / 2 ),
				Constraint.RelativeToView( driverImage, (p,v) => p.Width - ( v.X + v.Width + 10 + 10 ) ),
				Constraint.RelativeToView( driverImage, (p,v) => v.Height / 2 )
			);

			timeLabel = new Label () { 
				Text = "Hora aproximada de llegada",
				TextColor = Settings.Color5,
				FontSize = 14,
				XAlign = TextAlignment.Center
			};

			modeLayout.Children.Add (
				timeLabel,
				Constraint.Constant( 0 ),
				Constraint.RelativeToView( driverImage, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 18 )
			);

			arrivalLabel = new Label () { 
				Text = "---",
				TextColor = Settings.Color5,
				FontSize = 18,
				XAlign = TextAlignment.Center
			};

			modeLayout.Children.Add (
				arrivalLabel,
				Constraint.Constant( 0 ),
				Constraint.RelativeToView( timeLabel, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 22 )
			);

			callButton = new Button()
			{
				Text = "Llamar a conductor",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Settings.Color5,
				BackgroundColor = Settings.Color3,
				BorderRadius = 0
			};

			modeLayout.Children.Add(
				callButton,
				Constraint.RelativeToParent((p) => p.Width * 0.1),
				Constraint.RelativeToView(arrivalLabel, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent((p) => p.Width * 0.8),
				Constraint.Constant(40)
			);

			cancelButton = new Button () { 
				Text = "Cancelar",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Settings.Color5,
				BackgroundColor = Settings.Color3,
				BorderRadius = 0
			};

			modeLayout.Children.Add (
				cancelButton,
				Constraint.RelativeToParent( (p) => p.Width * 0.1 ),
				Constraint.RelativeToView( callButton, (p,v) => v.Y + v.Height + 10 ),
				Constraint.RelativeToParent( (p) => p.Width * 0.8 ),
				Constraint.Constant( 40 )
			);

			layout.Children.Add (
				modeLayout,
				Constraint.Constant( 0 ),
				Constraint.RelativeToParent( (p) => p.Height - 230 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.Constant( 230 )
			);

			callButton.Clicked += CallDriver;
			cancelButton.Clicked += CancelCab;
			LoadCab ().ConfigureAwait ( false );
		}

		private async Task LoadCab()
		{
			
		}

		private async Task WaitingModeAppear()
		{
			driverImage.IsVisible = false;
			driverLavel.IsVisible = false;
			tagLabel.IsVisible = false;
			timeLabel.IsVisible = false;
			arrivalLabel.IsVisible = false;
			callButton.IsVisible = false;

			modeLayout.TranslationY = modeLayout.Height;
			loadingIndicator.Opacity = 0;
			loadingLabel.Opacity = 0;
			driverImage.Opacity = 0;
			driverLavel.Opacity = 0;
			tagLabel.Opacity = 0;
			timeLabel.Opacity = 0;
			arrivalLabel.Opacity = 0;
			callButton.Opacity = 0;
			cancelButton.Opacity = 0;

			await modeLayout.TranslateTo ( 0, 0, 560 );
			await loadingIndicator.FadeTo ( 1 );
			await loadingLabel.FadeTo ( 1 );
			await driverImage.FadeTo( 1 );
			await driverLavel.FadeTo( 1 );
			await tagLabel.FadeTo( 1 );
			await timeLabel.FadeTo( 1 );
			await arrivalLabel.FadeTo( 1 );
			await callButton.FadeTo(1);
			await cancelButton.FadeTo( 1 );
		}

		private async Task WaitingModeDissapear()
		{
			await driverImage.FadeTo( 0 );
			await driverLavel.FadeTo( 0 );
			await tagLabel.FadeTo( 0 );
			await timeLabel.FadeTo( 0 );
			await arrivalLabel.FadeTo( 0 );
			await callButton.FadeTo(0);
			await cancelButton.FadeTo( 0 );
			await modeLayout.TranslateTo ( 0, modeLayout.Height, 560 );
		}
	}
}


