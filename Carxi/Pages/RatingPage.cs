﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Carxi
{
	public partial class RatingPage : ContentPage
	{
		private string tripId;
		private string fare;

		public RatingPage ( Dictionary<string,object> args )
		{
			tripId = args["trip"].ToString();
			fare = args["fare"].ToString();
			InitializeComponents ();
		}

		private void Cancel (object sender, EventArgs e)
		{
			Navigation.PopModalAsync ();
		}

		private void SendRate (object sender, EventArgs e)
		{
			if( tripRating.RateValue == 0 ) {
				DisplayAlert ("Carxi","Por favor seleccione el nivel de satisfacción","Aceptar");
				return;
			}

			CarxiService.Instance.RateTrip ( tripId, tripRating.RateValue );
			Navigation.PopModalAsync ();
		}
	}
}


