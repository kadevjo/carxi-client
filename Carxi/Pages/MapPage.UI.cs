﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

using Kadevjo.Dependencies;
using BindableMapTest.Extensions;
using BindableMapTest.Controls;

namespace Carxi
{
	public partial class MapPage : ContentPage
	{
		private RelativeLayout layout;
		private RelativeLayout modeLayout;
		private ExtendedMap cabsMap;
		private Pin userPin;
		private ToolbarItem focusButton;
		private ScrollView scrollView;

		private void InitializeComponents()
		{
			focusButton = new ToolbarItem ();
			focusButton.Text = "";
			focusButton.Icon = "ic_focus.png";
			focusButton.Clicked += SetUserLocation;
			ToolbarItems.Add (focusButton);

			layout = new RelativeLayout ();

			MapSpan span = MapSpan.FromCenterAndRadius (
				new Position (
					Settings.DefaultLocation.Latitude, 
					Settings.DefaultLocation.Longitude
				), 
				Distance.FromMiles (5)
			);

			cabsMap = new ExtendedMap ( span ) { 
				HasScrollEnabled = true,
				HasZoomEnabled = true,
				MapType = MapType.Street
			};

			layout.Children.Add ( 
				cabsMap,
				Constraint.Constant( 0 ),
				Constraint.Constant( 0 ),
				Constraint.RelativeToParent( (p) => p.Width ),
				Constraint.RelativeToParent( (p) => p.Height )
			);

			scrollView = new ScrollView () {
				Content = layout
			};

			Content = scrollView;
		}

		private async Task SetUserLocation()
		{
			var location = await DependencyService.Get<IToolsService>().GetUserLocation();
			double latitude = 0.0;
			double longitude = 0.0;
			location.TryGetValue( "Latitude", out latitude );
			location.TryGetValue( "Longitude", out longitude );
			cabsMap.MoveToRegion( MapSpan.FromCenterAndRadius(
				new Position(latitude, longitude),
				Distance.FromMiles(3))
			);

			// Refreshing user location
			if( userPin != null && cabsMap.Pins.Contains( userPin ) ) {
				cabsMap.Pins.Remove ( userPin );
			}

			ExtendedPin pin = new ExtendedPin() {
				Name = "Me",
				Details = "",
				Location = new Location( latitude, longitude ),
				PinIcon = "ic_mark"
			};

			userPin = pin.AsPin ();

			cabsMap.Items.Add ( pin );
			cabsMap.Pins.Add ( userPin );
		}
	}
}


