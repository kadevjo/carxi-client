﻿using System;
using System.Text.RegularExpressions;

using Xamarin.Forms;

namespace Carxi
{
	public partial class VerificationPage : ContentPage
	{
		public VerificationPage ()
		{
			Title = "Verificación";
			BackgroundColor = Color.White;
			InitializeComponents ();
		}

		private void VerifyPhone (object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty (codeEntry.Text) ||
				!Regex.IsMatch (codeEntry.Text, "^[0-9]{3}-[0-9]{3}-[0-9]{4}$")) {
				DisplayAlert ("Carxi", "Por favor ingrese un número de teléfono válido", "Aceptar");
				return;
			} 
			else if (App.Current.Properties.ContainsKey (Settings.PhoneKey)) {
				App.Current.Properties [Settings.PhoneKey] = codeEntry.Text;
				App.Current.SavePropertiesAsync();
			} 
			else {
				App.Current.Properties.Add ( Settings.PhoneKey, codeEntry.Text );
				App.Current.SavePropertiesAsync();
			}

			string phone = codeEntry.Text.Replace ("-", string.Empty);
			MessagingCenter.Send<Application,string> ( App.Current, Settings.PhoneAddedKey, phone );
			Navigation.PopModalAsync ();
		}

		private void CancelVerification (object sender, EventArgs e)
		{
			Navigation.PopModalAsync ();
		}

		private void EntryFocused (object sender, FocusEventArgs e)
		{
			scrollView.ScrollToAsync ( codeEntry,ScrollToPosition.Center, true );
		}

		private void EntryChanged (object sender, TextChangedEventArgs e)
		{
			Entry entry = (Entry)sender;

			if( !Regex.IsMatch (codeEntry.Text, "^[0-9-]{1,12}$") ) {
				entry.Text = e.OldTextValue;
			}
			else if( e.NewTextValue.Length == 3 ) {
				entry.Text += "-";
			}
			else if( e.NewTextValue.Length == 7 ) {
				entry.Text += "-";
			}
		}
	}
}


