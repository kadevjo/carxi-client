﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

using Kadevjo.Dependencies;
using System.Threading.Tasks;

using Apps = Xamarin.Forms.Application;

namespace Carxi
{
	public partial class MapPage : ContentPage
	{
		private TripResponse currentTrip;

		public MapPage ()
		{
			Title = "Carxi";
			InitializeComponents ();
			InitializeViewMode ();

			MessagingCenter.Subscribe<Apps,Dictionary<string,object>> (
				this, 
				Settings.TripEndKey, 
				TripEnded
			);
		}

		private async void SetUserLocation (object sender, EventArgs e)
		{
			await SetUserLocation ();
		}

		private void TripEnded( Application app, Dictionary<string,object> data )
		{
			Navigation.PushModalAsync ( new RatingPage( data ) );
			InitializeViewMode();
		}

		#region View Mode

		private async void RequestCab (object sender, EventArgs e)
		{
			if( !App.Current.Properties.ContainsKey( Settings.PhoneKey ) ) {
				var phone = DependencyService.Get<IToolsService> ().GetPhoneNumber ();
				if (string.IsNullOrEmpty (phone)) {
					Navigation.PushModalAsync (new VerificationPage ());
					return;
				} 
				else {
					App.Current.Properties.Add ( Settings.PhoneKey, phone );
 					App.Current.SavePropertiesAsync();
				}
			}

			await ViewModeDisappear ();
			InitializeRequestMode ();
			await RequestModeAppear ();

		}

		#endregion

		#region Request mode

		private void EntryFocused (object sender, FocusEventArgs e)
		{
			scrollView.ScrollToAsync ( (Element)sender, ScrollToPosition.Center, true );
		}

		private async void SelectAddress (object sender, EventArgs e)
		{
			if( string.IsNullOrEmpty( addressEntry.Text ) ) {
				DisplayAlert ("Carxi","Por favor ingrese una dirección válida","Aceptar");
				return;
			}

			Button button = (Button)sender;
			button.IsEnabled = false;

			var response = await CarxiService.Instance.CreateTrip (
				(string)App.Current.Properties[ Settings.PhoneKey ],
				addressEntry.Text
			);

			if (response == null || !response.Success) {
				string message = response != null ? response.Error : "Por favor intente nuevamente en unos minutos";
				DisplayAlert ("Carxi", message, "Aceptar");
				button.IsEnabled = true;
				return;
			} 
			else {
				currentTrip = new TripResponse ();
				currentTrip.Id = response.Trip;
			}

			await RequestModeDissapear ();
			InitializeWaitingMode ();
			await WaitingModeAppear ();
			PollTimer ( currentTrip.Id );
		}

		#endregion

		#region Waiting mode

		private void PollTimer( string id )
		{
			Device.StartTimer (
				TimeSpan.FromSeconds( 5 ),
				() => {
					if( currentTrip != null && currentTrip.Assigned ) {
						return false;
					}
					else {
						PollTrip( id ).ConfigureAwait( false );
						return true;
					}
				}
			);
		}

		private async Task PollTrip( string id )
		{
			currentTrip = await CarxiService.Instance.GetTrip( id );
			if( currentTrip != null && currentTrip.Assigned ) {
				loadingIndicator.IsVisible = false;
				loadingLabel.IsVisible = false;
				driverImage.IsVisible = true;
				driverLavel.IsVisible = true;
				tagLabel.IsVisible = true;
				timeLabel.IsVisible = true;
				arrivalLabel.IsVisible = true;
				callButton.IsVisible = true;

				driverLavel.Text = currentTrip.Cab.Description;
				tagLabel.Text = currentTrip.Cab.Tag;
				arrivalLabel.Text = currentTrip.PickupAt;
				driverImage.Source = currentTrip.Cab.DriverPicture;
			}
		}

		private async void CallDriver (object sender, EventArgs e)
		{
			Button button = (Button)sender;
			button.IsEnabled = false;

			var response = await CarxiService.Instance.CallDriver(currentTrip.Id);
			string message = response ? "El conductor se comunicará con ud en breve" : 
				"Por favor intente nuevamente en unos minutos";
			await DisplayAlert("Carxi", message, "Aceptar");

			button.IsEnabled = true;
		}

		private async void CancelCab (object sender, EventArgs e)
		{
			Button button = (Button)sender;
			button.IsEnabled = false;

			var response = await CarxiService.Instance.CancelTrip ( currentTrip.Id );

			if( response == null || !response.Success ) {
				string message = response != null ? response.Error : 
					"Por favor intente nuevamente en unos minutos";
				await DisplayAlert ( "Carxi", message, "Aceptar" );
				button.IsEnabled = true;
				return;
			}

			await WaitingModeDissapear ();
			InitializeViewMode ();
			await ViewModeAppear ();
			SetUserLocation ();
		}

		#endregion
	}
}


