﻿using System;
using System.Threading;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace Carxi.Droid
{
	[Activity(MainLauncher = true, NoHistory = true, Theme = "@android:style/Theme.NoTitleBar", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView ( Resource.Layout.Splash );
			Timer timer = new Timer( (s) => {
				StartActivity(typeof(MainActivity));
				Finish();
			}, null, 1000, Timeout.Infinite);
		}
	}
}

