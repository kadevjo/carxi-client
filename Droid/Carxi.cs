﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;

using Xamarin.Forms;

using Parse;

using Apps = Xamarin.Forms.Application;
using Android.Media;

namespace Carxi.Droid
{
	[Application(Name = "carxidroid.Carxi")]
	public class Carxi : Android.App.Application, Android.App.Application.IActivityLifecycleCallbacks
	{
		private bool applicationOpened;

		public Carxi(IntPtr javaReference, JniHandleOwnership transfer) : base( javaReference, transfer )		
		{
		}

		public override void OnCreate ()
		{
			base.OnCreate ();
			RegisterActivityLifecycleCallbacks(this);

			ParseClient.Initialize (Settings.ParseAppId, Settings.ParseAppKey);
			ParsePush.ParsePushNotificationReceived += ProcessNotification;

			// This is for targeting pushes to this specific user
			ParseInstallation currentInstallation = ParseInstallation.CurrentInstallation;
			MessagingCenter.Subscribe<Apps,string> (this, Settings.PhoneAddedKey, (sender, phone) => {
				try {
					currentInstallation.Add("phone", phone);
					currentInstallation.SaveAsync();
				}
				catch ( Exception e ) {
					Console.WriteLine( e.Message );
				}
			});
		}

		private void ProcessNotification( object IntentSender, ParsePushNotificationEventArgs args )
		{
			var payload = new Dictionary<string,object> ( args.Payload );

			if ( applicationOpened ) {
				MessagingCenter.Send<Apps,Dictionary<string,object>> ( 
					App.Current, Settings.TripEndKey, payload 
				);
			} 
			else {
				CreateNotification ( "Carxi", "Tu trip ha terminado", payload);
			}
		}

		private void CreateNotification(string title, string description, Dictionary<string,object> payload)
		{
			var notificationManager = GetSystemService(Context.NotificationService) as NotificationManager;
			Notification.Builder builder = new Notification.Builder(ApplicationContext);

			App.Args = payload;
			Intent result = new Intent(ApplicationContext, typeof(SplashActivity));
			TaskStackBuilder stack = TaskStackBuilder.Create(ApplicationContext);
			stack.AddNextIntent(result);
			PendingIntent resultPendingIntent = stack.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);
			builder.SetContentIntent(resultPendingIntent);

			builder.SetContentTitle(title);
			builder.SetContentText(description);
			builder.SetSmallIcon(Resource.Drawable.icon);
			Bitmap icon = BitmapFactory.DecodeResource(Context.Resources, Resource.Drawable.icon);
			builder.SetLargeIcon(icon);
			builder.SetStyle(new Notification.BigTextStyle().BigText(description));
			builder.SetSound (RingtoneManager.GetDefaultUri(RingtoneType.Notification));

			notificationManager.Notify(1, builder.Build());
		}

		public void OnActivityCreated (Activity activity, Bundle savedInstanceState)
		{
			Console.WriteLine ( activity.LocalClassName + " Created" );
		}

		public void OnActivityDestroyed (Activity activity)
		{
			Console.WriteLine ( activity.LocalClassName + " Destroyed" );
		}

		public void OnActivityPaused (Activity activity)
		{
			Console.WriteLine ( activity.LocalClassName + " Paused" );
			applicationOpened = false;
		}

		public void OnActivityResumed (Activity activity)
		{
			Console.WriteLine ( activity.LocalClassName + " Resumed" );
			applicationOpened = true;
		}

		public void OnActivitySaveInstanceState (Activity activity, Bundle outState)
		{
			Console.WriteLine ( activity.LocalClassName + " Saved" );
		}

		public void OnActivityStarted (Activity activity)
		{
			Console.WriteLine ( activity.LocalClassName + " Started" );
		}

		public void OnActivityStopped (Activity activity)
		{
			Console.WriteLine ( activity.LocalClassName + " Stopped" );
		}
	}
}