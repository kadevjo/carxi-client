using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Telephony;

using Xamarin.Forms;
using Kadevjo.Droid.Dependencies;
using Kadevjo.Dependencies;
using Android.Content.Res;
using Xamarin.Geolocation;
using System.Threading.Tasks;

[assembly:Dependency(typeof(ToolsService))]
namespace Kadevjo.Droid.Dependencies
{
	public class ToolsService : IToolsService
    {
		public void ClearNotifications()
		{
			NotificationManager manager = NotificationManager.FromContext(Forms.Context);
			manager.CancelAll ();
		}

        public void RefreshActionBar(Page page)
        {
            //Doing this because Android likes to bully developers
        }

        public async Task<IDictionary<string, double>> GetUserLocation()
        {
            var locator = new Geolocator(Forms.Context) { DesiredAccuracy = 1000 };
            try
            {
                var position = await locator.GetPositionAsync(timeout: 10000);
                var dictionary = new Dictionary<string, double>();
                dictionary.Add("Latitude", position.Latitude);
                dictionary.Add("Longitude", position.Longitude);
                return dictionary;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Dictionary<string, double>();
            }
        }

		public string GetPhoneNumber()
		{
			/*var telephonyManager = (TelephonyManager)Forms.Context.GetSystemService(
				Android.Content.Context.TelephonyService
			);

			return telephonyManager.Line1Number;*/
			return string.Empty;
		}
    }
}