#pragma warning disable 1591
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

[assembly: Android.Runtime.ResourceDesignerAttribute("Carxi.Droid.Resource", IsApplication=true)]

namespace Carxi.Droid
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
			global::Xamarin.Forms.Platform.Resource.String.ApplicationName = global::Carxi.Droid.Resource.String.ApplicationName;
			global::Xamarin.Forms.Platform.Resource.String.Hello = global::Carxi.Droid.Resource.String.Hello;
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7f020000
			public const int ic_enter_pin = 2130837504;
			
			// aapt resource value: 0x7f020001
			public const int ic_focus = 2130837505;
			
			// aapt resource value: 0x7f020002
			public const int ic_invalid_pin = 2130837506;
			
			// aapt resource value: 0x7f020003
			public const int ic_mark = 2130837507;
			
			// aapt resource value: 0x7f020004
			public const int ic_order_taxi = 2130837508;
			
			// aapt resource value: 0x7f020005
			public const int ic_pin = 2130837509;
			
			// aapt resource value: 0x7f020006
			public const int ic_star_favoff = 2130837510;
			
			// aapt resource value: 0x7f020007
			public const int ic_star_favon = 2130837511;
			
			// aapt resource value: 0x7f020008
			public const int ic_taxi_location = 2130837512;
			
			// aapt resource value: 0x7f020009
			public const int icon = 2130837513;
			
			// aapt resource value: 0x7f02000a
			public const int splash = 2130837514;
			
			// aapt resource value: 0x7f02000b
			public const int taxi_picture = 2130837515;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f030000
			public const int Splash = 2130903040;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f040001
			public const int ApplicationName = 2130968577;
			
			// aapt resource value: 0x7f040000
			public const int Hello = 2130968576;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
		
		public partial class Style
		{
			
			// aapt resource value: 0x7f050001
			public const int CarxiActionBar = 2131034113;
			
			// aapt resource value: 0x7f050000
			public const int CarxiActionBarTheme = 2131034112;
			
			static Style()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Style()
			{
			}
		}
	}
}
#pragma warning restore 1591
